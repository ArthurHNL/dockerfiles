#!/bin/bash
echo "Going to sleep for 30 seconds to allow database to boot up."
sleep 30
echo "Came back from sleep"

echo "Going to migrate database"
php /var/www/artisan migrate --force || exit 1

echo "Migrated database"

echo "Going to put apache in the foreground."
exec apache2-foreground
