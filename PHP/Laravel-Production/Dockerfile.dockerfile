# This configuration expects a .dockerignore to ignore .gitignores, the vendor folder, the node_modules folder
# and other files you don't want in your application.
# Use Node.JS to copy applicatio the application files, install frontend dependencies and run webpack.
FROM node:latest AS js-env

    # Application files.
    COPY ./src /work

    # Frontend depdencies, remove files not used.
    RUN npm install && \
        npm run production && \
        rm package.json && \
        rm package-lock.json && \
        rm webpack.mix.js && \
        rm -rf node_modules

# Use composer to install backend PHP depdencies.
FROM arthurhnl/composer-prestissimo:latest AS php-dep-env
    COPY --from=js-env /work /work

    # PHP backend dependencies, remove files not used.
    RUN cd /work && \
        composer install --no-dev --optimize-autoloader && \
        rm composer.json && \
        rm composer.lock

# Use PHP version 7.2 with apache for the final image.
FROM php:7.2-apache

    # Environment Variables
    ENV APP_NAME=appName
        LOG_CHANNEL=stderr
        DEBIAN_FRONTEND=noninteractive

    # Runtime Dependencies
        # Debian packages for various purposes.
        RUN apt-get update -yqq && \
            apt-get upgrade -yqq --no-install-recommends && \
            apt-get install -yqq \
            libmcrypt-dev \
            mysql-client \
            xfonts-base \
            fontconfig \
            libjpeg62-turbo \
            libx11-6 \
            libxcb1 \
            libxext6 \
            libxrender1 \
            libxml2-dev \
            xfonts-75dpi \
            --no-install-recommends && \
            apt-get autoremove -yqq && \
            apt-get clean -qq

        # wkhtmltox for generating PDF files.
        RUN curl -L -o wkhtmltopdf.deb https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb && \
            test "$(sha256sum wkhtmltopdf.deb | awk '{print $1}')" = "1140b0ab02aa6e17346af2f14ed0de807376de475ba90e1db3975f112fbd20bb" && \
            dpkg -i wkhtmltopdf.deb && \
            rm wkhtmltopdf.deb

    # PHP Configuration
        # PHP.ini
        RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

        # PHP Extensions
        RUN docker-php-ext-install pdo_mysql soap

    # Application files
        COPY --from=php-dep-env /work /var/www

    # Webserver configuration
        COPY ./dockerfile/conf/apache-vhost.conf /etc/apache2/sites-available/000-default.conf

    # Permissions and a2enmod
        RUN chmod -R ugo+w /var/www/storage && \
        a2enmod rewrite

    # Script that migrates the database, and then puts apache in the foreground.
        COPY .start-production.sh /scripts/start-production.sh
        CMD ["/scripts/start-production.sh"]

    # Healtcheck
        HEALTHCHECK --interval=5s --timeout=1m \
            CMD curl -f http://localhost/api/healthcheck || exit 1

    # Working directory to allow the easy execution of artisan.
        WORKDIR /var/www
